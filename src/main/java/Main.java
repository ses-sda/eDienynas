import db.entity.Lesson;
import db.entity.Schedule;
import db.entity.Student;
import db.entity.Teacher;
import db.repository.LessonRepository;
import db.repository.ScheduleRepository;
import db.repository.StudentRepository;
import db.repository.TeacherRepository;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import javax.persistence.EntityManager;


public class Main implements AutoCloseable{
    private final EntityManager entityManager;
    private final StudentRepository studentRepository;
    private final TeacherRepository teacherRepository;
    private final LessonRepository lessonRepository;
    private final ScheduleRepository scheduleRepository;

    public Main(){
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Student.class)
                .addAnnotatedClass(Teacher.class)
                .addAnnotatedClass(Lesson.class)
                .addAnnotatedClass(Schedule.class)
                .buildSessionFactory();

        entityManager = sessionFactory.createEntityManager();
        studentRepository = new StudentRepository(entityManager);
        teacherRepository = new TeacherRepository(entityManager);
        lessonRepository = new LessonRepository(entityManager);
        scheduleRepository = new ScheduleRepository(entityManager);
    }

    public void runStudentsList(){
        System.out.println("========================================");
        Student newStudent = new Student();
//        newStudent.setFirstName("Maria");
//        newStudent.setLastName("Mendez");
//        studentRepository.addToList(newStudent);
        studentRepository.list().forEach(System.out::println);
        System.out.println("========================================");
        studentRepository.deleteFromList(5);
        studentRepository.list().forEach(System.out::println);
        System.out.println("========================================");
    }

    public void runTeachersList(){
        System.out.println("========================================");
        teacherRepository.list().forEach(System.out::println);
        System.out.println("========================================");
    }

    public void runLessonsList(){
        System.out.println("========================================");
        lessonRepository.list().forEach(System.out::println);
        System.out.println("========================================");
    }

    public void runScheduleList(){
        System.out.println("========================================");
        scheduleRepository.findScheduleById(1);
        scheduleRepository.list().forEach(System.out::println);
        System.out.println("========================================");
    }

    public static void main(String[] args) {
        try(Main main = new Main()){
            main.runStudentsList();
            main.runTeachersList();
            main.runLessonsList();
            main.runScheduleList();
        }
    }

    @Override
    public void close(){
       entityManager.close();
    }
}
