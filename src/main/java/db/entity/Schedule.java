package db.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "schedule")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private Lesson lesson;

    @OneToOne
    private Teacher teacher;

    @OneToOne
    private Student student;

    @Column(name = "datetime")
    private Date date;

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", lesson=" + lesson +
                ", teacher=" + teacher +
                ", student=" + student +
                ", date=" + date +
                '}';
    }
}
