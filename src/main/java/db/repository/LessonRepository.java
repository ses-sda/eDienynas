package db.repository;

import db.entity.Lesson;

import javax.persistence.EntityManager;

public class LessonRepository extends MethodsRepository<Lesson>{

    public LessonRepository(EntityManager entityManager) {
        super(entityManager, Lesson.class);
    }
}
