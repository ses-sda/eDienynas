package db.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Optional;

public class MethodsRepository<T> implements Repository<T>{
    protected final EntityManager entityManager;
    private final Class<T> entityClass;

    public MethodsRepository(EntityManager entityManager, Class<T> entityClass) {
        this.entityManager = entityManager;
        this.entityClass = entityClass;
    }

    @Override
    public List<T> list() {
        return entityManager.createQuery("FROM " + entityClass.getSimpleName(), entityClass).getResultList();
    }

    @Override
    public Optional<T> findById(int id) {
        return Optional.ofNullable(entityManager.find(entityClass, id));
    }

    @Override
    public void addToList(T entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
    }

    @Override
    public void deleteFromList(int id) {
        findById(id).ifPresent(entity -> {
            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.remove(entity);
            transaction.commit();
        });
    }
}
