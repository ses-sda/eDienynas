package db.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T>{

    List<T> list();

    Optional<T> findById(int id);

    void addToList(T entity);

    void deleteFromList(int id);
}
