package db.repository;

import db.entity.Schedule;

import javax.persistence.EntityManager;

public class ScheduleRepository extends MethodsRepository<Schedule>{
    public ScheduleRepository(EntityManager entityManager) {
        super(entityManager, Schedule.class);
    }

    public void findScheduleById(int id){
        findById(id - 1).ifPresent(schedule -> {
            entityManager.createQuery("SELECT s.date, Lesson.subject, Teacher.firstName, Teacher.lastName, " +
                    "Student.firstName, Student.lastName " +
                    "FROM Schedule s " +
                    "LEFT JOIN s.id, s.lesson, s.teacher, s.student", Schedule.class)
                    .getResultList();
        });
    }
}
