package db.repository;

import db.entity.Student;

import javax.persistence.EntityManager;

public class StudentRepository extends MethodsRepository<Student>{

    public StudentRepository(EntityManager entityManager) {
        super(entityManager, Student.class);
    }
}
