package db.repository;

import db.entity.Teacher;

import javax.persistence.EntityManager;

public class TeacherRepository extends MethodsRepository<Teacher> {

    public TeacherRepository(EntityManager entityManager) {
        super(entityManager, Teacher.class);
    }
}
